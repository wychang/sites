"""sites URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from mainsite.views import index,login,forgot,signup,blank,page404, \
                            chart,card_demo,example_color,example_border, \
                            example_other,example_animation,example_button,example_table

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index),
    path('login/', login),
    path('forgot/', forgot),
    path('signup/', signup),
    path('blank/', blank),
    path('page404/', page404),
    path('chart/', chart),
    path('card_demo/', card_demo),
    path('example_color/',example_color),
    path('example_border/',example_border),
    path('example_other/',example_other),
    path('example_animation/',example_animation),
    path('example_button/',example_button),
    path('example_table/',example_table),
]
