from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request,'pages/index.html',locals())

def login(request):
    return render(request,'pages/login.html')

def forgot(request):
    return render(request,'pages/forgot_password.html')

def signup(request):
    return render(request,'pages/register.html')

def blank(request):
    return render(request,'pages/blank.html')

def page404(request):
    return render(request,'pages/404.html')

def chart(request):
    return render(request,'pages/chart.html')

def card_demo(request):
    return render(request,'pages/card_demo.html')

def example_color(request):
    return render(request,'utilities/utilities-color.html')

def example_border(request):
    return render(request,'utilities/utilities-border.html')

def example_other(request):
    return render(request,'utilities/utilities-other.html')

def example_animation(request):
    return render(request,'utilities/utilities-animation.html')

def example_button(request):
    return render(request,'utilities/buttons.html')

def example_table(request):
    return render(request,'utilities/tables.html')